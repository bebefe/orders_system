﻿using OrdersSystem.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OrdersSystem
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=OrderSystemDB;Integrated Security=True";
            var manager = new DBStorageManager(connectionString);
            Storage.Instance.Initialize(manager);

            Application.Run(new MainForm());
        }
    }
}
