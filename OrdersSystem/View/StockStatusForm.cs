﻿using OrdersSystem.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OrdersSystem.View
{
    public partial class StockStatusForm : Form
    {
        private Storage Storage => Storage.Instance;

        private string Product => tbProduct.Text;
        private string Count => tbCount.Text;
        private string Price
        {
            get => tbPrice.Text;
            set => tbPrice.Text = value;
        }
        private bool IsValidDouble(string source)
        {
            try
            {
                if (double.Parse(source) < 0)
                    return false;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public StockStatusForm()
        {
            InitializeComponent();

            UpdateGrid();
            ClearInputs();
        }

        private void UpdateGrid()
        {
            var stockStatus = Storage.GetStockStatus();

            dataGridView1.Rows.Clear();

            foreach (var item in stockStatus)
            {
                dataGridView1.Rows.Add(item.Item1, item.Item2, item.Item3);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure?", "To menu", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                MainForm.Form.Show();
                this.Close();
            }
        }

        private void ClearInputs()
        {
            tbCount.Text = "0";
            tbPrice.Text = "0";
            tbProduct.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(Product))
            {
                MessageBox.Show("Incorrect product name");
                return;
            }

            if (IsValidDouble(Count) == false)
            {
                MessageBox.Show("Incorrect count");
                return;
            }

            if (IsValidDouble(Price) == false)
            {
                MessageBox.Show("Incorrect price");
                return;
            }


            if (MessageBox.Show("Are you sure?", "To menu", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Storage.AddProduct(Product, double.Parse(Count), double.Parse(Price));
                UpdateGrid();
                MessageBox.Show("Added");

                ClearInputs();
            }
        }

        private void tbProduct_TextChanged(object sender, EventArgs e)
        {
            if (Storage.GetKindNames().Contains(Product))
            {
                Price = Storage.GetProductPrice(Product).ToString();
            }
        }
    }
}
