﻿using OrdersSystem.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OrdersSystem.View
{
    public partial class ViewDeliveryForm : Form
    {
        private Storage Storage => Storage.Instance;
        private bool _isCorrectOrderNumber;
        private Order CurrentOrder;

        public bool IsCorrectOrderNumber
        {
            get => _isCorrectOrderNumber;
            private set
            {
                _isCorrectOrderNumber = value;
                labelValidCount.Visible = !value;
                button1.Enabled = value;
            }
        }

        public int OrderNumber => IsCorrectOrderNumber ? int.Parse(comboBox1.Text) : int.MinValue;

        public ViewDeliveryForm()
        {
            InitializeComponent();

            InitCombobox();

            FillOrderInfo();
        }

        private void InitCombobox()
        {
            comboBox1.Items.Clear();

            var orders = Storage.GetAllOrdersNumbers().Select(x => x.ToString()).ToArray();

            comboBox1.Items.AddRange(orders);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure?", "To menu", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                MainForm.Form.Show();
                this.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            UpdateOrder();
        }

        private void UpdateOrder()
        {
            CurrentOrder = Storage.GetOrderByID(OrderNumber);

            if (CurrentOrder is null)
            {
                MessageBox.Show("We dont have order with this number");
                return;
            }


            FillOrderInfo();
        }

        private void FillOrderInfo()
        {
            if (CurrentOrder is null)
            {
                ClearOrderInfo();
                return;
            }

            tbStatus.Text = CurrentOrder.Status.ToString();
            tbAddress.Text = CurrentOrder.Address;
            tbDeliver.Text = CurrentOrder.Deliver.Name;
            tbProduct.Text = CurrentOrder.Product;
            tbTime.Text = CurrentOrder.DeliveryDate.ToString();

            tbCount.Text = CurrentOrder.Quantity.ToString();
            tbPrice.Text = CurrentOrder.TotalPrice.ToString();

            labelNumber.Text = CurrentOrder.ID.ToString();

            btnCancelOrder.Enabled = CurrentOrder.Status == OrderStatus.Active;
            btnCloseOrder.Enabled = CurrentOrder.Status == OrderStatus.Active;
            label8.Visible = true;
            labelNumber.Visible = true;
        }

        private void ClearOrderInfo()
        {
            tbAddress.Text = tbDeliver.Text = tbProduct.Text = tbStatus.Text = tbCount.Text = tbPrice.Text = tbTime.Text = "";
            btnCancelOrder.Enabled = false;
            btnCloseOrder.Enabled = false;
            label8.Visible = false;
            labelNumber.Visible = false;
        }


        private void btnCloseOrder_Click(object sender, EventArgs e)
        {
            Storage.CloseOrder(CurrentOrder);
            MessageBox.Show("Success");
            FillOrderInfo();
        }

        private void btnCancelOrder_Click(object sender, EventArgs e)
        {
            Storage.CancelOrder(CurrentOrder);
            MessageBox.Show("Success");
            FillOrderInfo();
        }

        private void CorrectOrderCheck()
        {

            IsCorrectOrderNumber = true;
            try
            {
                int.Parse(comboBox1.Text);
            }
            catch
            {
                IsCorrectOrderNumber = false;
            }
        }
        private void comboBox1_TextChanged(object sender, EventArgs e)
        {
            CorrectOrderCheck();
        }
        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            CorrectOrderCheck();
            UpdateOrder();
        }
    }
}
