﻿using OrdersSystem.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OrdersSystem.View
{
    public partial class MakingOrderForm : Form
    {
        private bool isValidCount;

        private Storage Storage => Storage.Instance;
        public MakingOrderForm()
        {
            InitializeComponent();

            FillComboboxWithKind();
            labelTotalPrice.Text = "";
        }

        private void FillComboboxWithKind()
        {
            var values = Storage.GetKindNames().Select(x => x as object).ToArray();

            comboBox1.Items.Clear();
            comboBox1.Items.AddRange(values);
            comboBox1.SelectedIndex = 0;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want cancel?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                MainForm.Form.Show();
                this.Close();
            }
        }

        public bool IsValidCount
        {
            get => isValidCount;
            private set
            {
                isValidCount = value;
                labelValidCount.Visible = !value;
                if (!value)
                    labelTotalPrice.Text = "";
            }
        }
        public string ProductKind
        {
            get
            {
                return (string)comboBox1.SelectedItem;
            }
        }
        public double Count => IsValidCount ? double.Parse(tbCount.Text) : double.MinValue;
        public DateTime DeliveryDate => dateTimePicker1.Value;
        public string Address => tbAddress.Text;

        private void button1_Click(object sender, EventArgs e)
        {
            if (IsValidCount == false)
            {
                MessageBox.Show("Invalid count field");
                return;
            }

            var number = Storage.AddOrder(ProductKind, Count, DeliveryDate, Address);
            MessageBox.Show($"Great! Your order number is {number}, remember it.");
            MainForm.Form.Show();
            Close();
        }

        public double TotalPrice
        {
            get
            {
                return double.Parse(labelTotalPrice.Text.Substring(0, labelTotalPrice.Text.Length - 2));
            }
            set
            {
                labelTotalPrice.Text = value.ToString() + " ₴";
            }
        }
        public double ItemPrice
        {
            get
            {
                return double.Parse(labelItemPrice.Text.Substring(0, labelItemPrice.Text.Length - 2));
            }
            set
            {
                labelItemPrice.Text = value.ToString() + " ₴";
            }
        }
          
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ItemPrice = Storage.GetProductPrice(ProductKind);

            if (IsValidCount)
            {
                TotalPrice = Storage.GetTotalPrice(ProductKind, Count); 
            }
        }

        void TextChanged()
        {
            IsValidCount = true;
            try
            {
                double.Parse(tbCount.Text);
                TotalPrice = Storage.GetTotalPrice(ProductKind, Count);
            }
            catch
            {
                IsValidCount = false;
            }
        }
        private void tbCount_TextChanged(object sender, EventArgs e)
        {
            TextChanged();
        }

        private void comboBox1_TextChanged(object sender, EventArgs e)
        {
            TextChanged();
        }
    }
}
