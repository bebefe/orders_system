﻿using OrdersSystem.Model;
using OrdersSystem.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OrdersSystem
{
    public partial class MainForm : Form
    {
        public static MainForm Form;
        public static Storage Storage => Storage.Instance;
        public MainForm()
        {
            InitializeComponent();
            Form = Form ?? this;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var makingOrderForm = new MakingOrderForm(); 
            makingOrderForm.Show(); 
            Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var stockStatusForm = new StockStatusForm();
            stockStatusForm.Show();
            Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var viewDeliveryForm = new ViewDeliveryForm();
            viewDeliveryForm.Show();
            Hide();
        }
    }
}
