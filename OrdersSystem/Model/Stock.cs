﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrdersSystem.Model
{
    public class Stock
    {
        public Dictionary<string, List<(double, DateTime)>> ProductsCount = new Dictionary<string, List<(double, DateTime)>>();
        public Dictionary<string, double> ProductsPrice = new Dictionary<string, double>();

        public IEnumerable<(string, double, double)> Status => ProductsCount.Join(ProductsPrice, x => x.Key, x => x.Key, (x, y) => (x.Key, GetCount(x.Key), y.Value));

        private double GetCount(string key)
        {
            return ProductsCount.Where(x => x.Key == key).Sum(x => x.Value.Sum(xx => xx.Item1));
        }

        public IEnumerable<string> GetKindNames()
        {
            return ProductsCount.Select(x => x.Key).Union(ProductsPrice.Select(x => x.Key));
        }
        public void Add(string product, double count, DateTime productionDate)
        {
            if (ProductsCount.ContainsKey(product) == false)
            {
                ProductsCount.Add(product, new List<(double, DateTime)>());
            }
            ProductsCount[product].Add((count, productionDate));
        }
        public void Add(string product, double count, DateTime productionDate, double price)
        {
            Add(product, count, productionDate);
            AddPrice(product, price); 
        }

        public void AddPrice(string product, double price)
        {
            ProductsPrice[product] = price;
        }
    }
}