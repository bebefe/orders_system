﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OrdersSystem.Model
{
    public enum OrderStatus
    {
        Active,
        Closed,
        Canceled
    }

    public class Order
    {
        public Order(string productKind, double count, DateTime deliveryDate, string address)
        {
            Product = productKind;
            Quantity = count;
            DeliveryDate = deliveryDate;
            Address = address;
            CreationDate = DateTime.Now;
            Status = OrderStatus.Active;
        }
        public OrderStatus Status { get; set; }
        public int ID { get; set; }
        public string Product { get; set; }
        public Deliver Deliver { get; set; }
        public double Quantity { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime DeliveryDate { get; set; }
        public string Address { get; set; }
        public double TotalPrice { get; set; }
    }
}
