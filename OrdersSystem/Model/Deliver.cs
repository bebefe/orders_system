﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrdersSystem.Model
{
    public class Deliver
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public Deliver(string name)
        {
            Name = name;
        }
    }
}
