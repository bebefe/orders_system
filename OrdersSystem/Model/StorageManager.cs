﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrdersSystem.Model
{
    public abstract class StorageManager
    {
        protected Stock Stock;
        protected OrdersManager OrdersManager;
        public abstract void Initialize(Stock stock, OrdersManager orders);
        public abstract void AddOrder(Order order);
        public abstract void AddProduct(string product, double count, DateTime date, double price);
        public abstract void ChangeOrderStatus(Order order, OrderStatus status);
    }
}
