﻿using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters;
using System.Text;
using System.Threading.Tasks;

namespace OrdersSystem.Model
{
    public class Storage
    {
        private readonly Stock Stock;
        private readonly OrdersManager ordersManager;

        public StorageManager Manager;
        private static Storage _instance;
        public static Storage Instance => _instance;
        private Storage()
        {
            Stock = new Stock();
            ordersManager = new OrdersManager();
        }

        public IEnumerable<(string, double, double)> GetStockStatus()
        {
            return Stock.Status;
        }

        static Storage()
        {
            _instance = new Storage();
        }

        public IEnumerable<string> GetKindNames()
        {
            return Stock.GetKindNames();
        }

        public double GetProductPrice(string product)
        {
            return Stock.ProductsPrice[product];
        }
        public void Initialize(StorageManager manager)
        {
            Manager = manager;
            manager.Initialize(Stock, ordersManager);
        }

        public  IEnumerable<int> GetAllOrdersNumbers()
        {
            return ordersManager.Orders.Select(x => x.ID);
        }

        public int AddOrder(string productKind, double count, DateTime deliveryDate, string address)
        { 
            var order = new Order(productKind, count, deliveryDate, address);
            order.TotalPrice = Stock.ProductsPrice[productKind] * count;
            Manager.AddOrder(order);
            return order.ID;
        }

        public Order GetOrderByID(int orderNumber)
        {
            return ordersManager.Orders.SingleOrDefault(x => x.ID == orderNumber);
        }

        public double GetTotalPrice(string productKind, double count)
        {
            return Stock.ProductsPrice[productKind] * count;
        }

        public void AddProduct(string product, double count, double price)
        {
            Manager.AddProduct(product, count, DateTime.Now, price);
        }

        public void CloseOrder(Order order)
        {
            Manager.ChangeOrderStatus(order, OrderStatus.Closed);
        }
        public void CancelOrder(Order order)
        {
            Manager.ChangeOrderStatus(order, OrderStatus.Canceled); 
        }
    }
}
