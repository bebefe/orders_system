﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrdersSystem.Model
{
    public class OrdersManager
    {
        private List<Order> _orders;
        private List<Deliver> _delivers;

        public IReadOnlyList<Order> Orders => _orders;
        public IReadOnlyList<Deliver> Delivers => _delivers;

        public OrdersManager()
        {
            _orders = new List<Order>();
            _delivers = new List<Deliver>();
        }

        public void AddDeliver(Deliver deliver)
        {
            _delivers.Add(deliver);
        }
        public void AddOrder(Order order)
        {
            order.Deliver = _delivers[new Random(new object().GetHashCode()).Next(_delivers.Count)];
            _orders.Add(order); 
        }
         
        public void ChangeOrderStatus(Order order, OrderStatus status)
        {
            order.Status = status;
        }
    }
}
