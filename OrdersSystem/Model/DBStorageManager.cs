﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace OrdersSystem.Model
{
    public static class EX
    {
        public static string ToSQL(this DateTime date)
        {
            return $"CONVERT(datetime, '{date.ToString("yyyy-MM-dd HH:mm:ss.fff")}', 120)";
        }
    }
    class DBStorageManager : StorageManager, IDisposable
    {
        protected string ConnectionString;
        protected SqlConnection connection;

        public DBStorageManager(string connection)
        {
            ConnectionString = connection;
        }
        public override void AddOrder(Order order)
        {
            OrdersManager.AddOrder(order);
            var insertExpr = $"INSERT INTO Orders  VALUES ((select id from Products where name = '{order.Product}'),{order.Quantity}, {order.Deliver.ID}, {order.TotalPrice}, '{order.Address}',  {order.CreationDate.ToSQL()}, {order.DeliveryDate.ToSQL()}, {(int)order.Status})";
            var insertCommand = new SqlCommand(insertExpr, connection);
            insertCommand.ExecuteNonQuery();
             
            var getIdExpr = "SELECT TOP 1 id FROM Orders ORDER BY ID DESC";
            ExecuteReader(getIdExpr, x => order.ID = x.GetInt32(0)); 
          
        }

        public override void Initialize(Stock stock, OrdersManager orders)
        {
            Stock = stock;
            OrdersManager = orders;
            connection = new SqlConnection(ConnectionString);
            connection.Open();
            FillDelivers();
            FillStock();
            FillOrders();
        }

        private void FillOrders()
        {
            string sqlExpression = "select o.id,name, quantity,  total_price, address, delivery_date, deliver_id, creation_date,  status from Orders o inner join Products p on o.product_id = p.id";
            ExecuteReader(sqlExpression, x =>
            {
                var id = x.GetInt32(0);
                var name = x.GetString(1);
                var count = x.GetDouble(2);
                var totalPrice = x.GetDouble(3);
                var address = x.GetString(4);
                var deliveryDate = x.GetDateTime(5);
                var deliverId = x.GetInt32(6);
                var creationDate = x.GetDateTime(7);
                var status = (OrderStatus)x.GetInt32(8);
                var order = new Order(name, count, deliveryDate, address)
                {
                    TotalPrice = totalPrice,
                    Status = status,
                    CreationDate = creationDate,
                    ID = id
                };
                OrdersManager.AddOrder(order);
                order.Deliver = OrdersManager.Delivers.Single(xx => xx.ID == deliverId);
            });
        }

        private void FillStock()
        {
            string sqlExpression = "select name, price, count, production_date from Products p left join ProductsAvailability pa on p.id = pa.product_id";
            ExecuteReader(sqlExpression, x =>
            {
                var product = x.GetString(0);
                var price = x.GetDouble(1);
                double count = 0;
                if (x.IsDBNull(2) == false)
                    count = x.GetDouble(2);
                DateTime prodDate = new DateTime();
                if (x.IsDBNull(3) == false)
                    prodDate = x.GetDateTime(3);
                Stock.Add(product, (double)count, (DateTime)prodDate, price);
            });
        }


        private void FillDelivers()
        {
            string sqlExpression = "SELECT * FROM Delivers";
            ExecuteReader(sqlExpression, x =>
            {
                var id = x.GetInt32(0);
                var name = x.GetString(1);
                var deliver = new Deliver(name) { ID = id };
                OrdersManager.AddDeliver(deliver);
            });
        }

        private void ExecuteReader(string expr, Action<SqlDataReader> action)
        {
            var command = new SqlCommand(expr, connection);
            var reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    action(reader);
                }
            }
            reader.Close();
        }

        public override void AddProduct(string product, double count, DateTime date, double price)
        {
            var updateExpr = $"UPDATE Products   SET price = {price} WHERE name = '{product}'";
            var updateCommand = new SqlCommand(updateExpr, connection);
            var res = updateCommand.ExecuteNonQuery();

            if (res == 0)
            {
                var insertExpr = $"INSERT INTO Products  VALUES ('{product}', {price})";
                var insertCommand = new SqlCommand(insertExpr, connection);
                insertCommand.ExecuteNonQuery();
            }

            var insertStockExpr = $"INSERT INTO ProductsAvailability VALUES((select id from Products where name = '{product}'), {count},   {date.ToSQL()} )";
            var command = new SqlCommand(insertStockExpr, connection);
            command.ExecuteNonQuery();

            Stock.Add(product, count, DateTime.Now, price);
        }

        public override void ChangeOrderStatus(Order order, OrderStatus status)
        {
            var sqlExpression = $"UPDATE Orders   SET status = {(int)status} WHERE id = {order.ID}";
            var command = new SqlCommand(sqlExpression, connection);
            command.ExecuteNonQuery();
            OrdersManager.ChangeOrderStatus(order, status);
        }

        public void Dispose()
        {
            connection.Dispose();
        }
    }
}
